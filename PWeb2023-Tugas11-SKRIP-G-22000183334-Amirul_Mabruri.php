<!DOCTYPE html>
<html>
<head>
    <title>Form Mahasiswa</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        
        .container {
            max-width: 400px;
            margin: 0 auto;
        }
        
        h2 {
            text-align: center;
            margin-top: 50px;
        }
        
        form {
            padding: 20px;
            background-color: #f2f2f2;
            border-radius: 5px;
        }
        
        .form-group {
            margin-bottom: 15px;
        }
        
        .form-group label {
            display: inline-block;
            width: 120px;
            font-weight: bold;
        }
        
        .form-group input[type="text"],
        .form-group select {
            width: 250px;
            padding: 5px;
            border-radius: 3px;
            border: 1px solid #ccc;
        }
        
        .form-group .error {
            color: #ff0000;
        }
        
        .form-group input[type="submit"] {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            padding: 10px 20px;
            cursor: pointer;
            border-radius: 3px;
        }
        
        .form-group input[type="submit"]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>Form Mahasiswa</h2>
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <div class="form-group">
                <label for="nama">Nama:</label>
                <input type="text" id="nama" name="nama" required>
            </div>
            <div class="form-group">
                <label for="nim">NIM:</label>
                <input type="text" id="nim" name="nim" required>
            </div>
            <div class="form-group">
                <label for="jurusan">Jurusan:</label>
                <select id="jurusan" name="jurusan" required>
                    <option value="">Pilih Jurusan</option>
                    <option value="Teknik Informatika">Teknik Informatika</option>
                    <option value="Sistem Informasi">Sistem Informasi</option>
                    <option value="Teknik Elektro">Teknik Elektro</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" value="Submit">
            </div>
        </form>
    </div>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nama = $_POST["nama"];
    $nim = $_POST["nim"];
    $jurusan = $_POST["jurusan"];
    
    // Validasi PHP
    $errors = array();
    
    if (empty($nama)) {
        $errors["nama"] = "Nama harus diisi";
    }
    
    if (empty($nim)) {
        $errors["nim"] = "NIM harus diisi";
    }
    
    if (empty($jurusan)) {
        $errors["jurusan"] = "Jurusan harus dipilih";
    }
    
    // Jika tidak ada error, lakukan proses selanjutnya
    if (empty($errors)) {
        // Proses selanjutnya, contoh: menyimpan data ke database
        // ...
        
        // Setelah proses selesai, redirect ke halaman lain atau tampilkan pesan sukses
        echo "Data berhasil disimpan!";
        exit;
    }
}
?>

</body>
</html>
